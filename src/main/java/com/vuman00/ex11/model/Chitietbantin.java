package com.vuman00.ex11.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.sql.Date;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Data
public class Chitietbantin implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idchitiet;

    private String tieude;
    private String noidung;
    private  String tomtat;
    private  int solanxem;
    private Date ngaydang;
    private String lkanh;
    private int iddanhmuc;

}

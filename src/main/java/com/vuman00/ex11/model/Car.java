package com.vuman00.ex11.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.hibernate.annotations.DiscriminatorOptions;

import javax.persistence.*;
import java.io.Serializable;

@Data
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})

@Entity
@DiscriminatorColumn(name = "car_type")
@DiscriminatorOptions(force=true)

@Table(name = "car")
public abstract class Car implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idcar;

    private String carname;
    private  String numberplate;
    private  int yearmanufature;
    private String brand;
    private int haveisurance;
}

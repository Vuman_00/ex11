package com.vuman00.ex11.api;


import com.vuman00.ex11.model.Chitietbantin;
import com.vuman00.ex11.repository.BaivietRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController

@RequestMapping("/api/baiviet")
public class BaivietApi {
    public static Logger logger = LoggerFactory.getLogger(BaivietApi.class);

    @Autowired
    BaivietRepository baivietRepository;

    @RequestMapping(value = "/", method = RequestMethod.GET )
    public ResponseEntity<List<Chitietbantin>> listAllContact(){

        List<Chitietbantin> chitietbantins = baivietRepository.findAll();
        if(chitietbantins.isEmpty()) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Chitietbantin>>(chitietbantins, HttpStatus.OK);
    }
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Chitietbantin findBaiviet(@PathVariable("id") int id) {
        Chitietbantin chitietbantin = baivietRepository.getOne(id);
        if(chitietbantin == null) {
            ResponseEntity.notFound().build();
        }
        return chitietbantin;
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public Chitietbantin saveContact(@Valid @RequestBody Chitietbantin contact) {
        return baivietRepository.save(contact);
    }

    @RequestMapping(value = "/", method = RequestMethod.PUT)
    public ResponseEntity<Chitietbantin> updateBaiviet(@PathVariable(value = "id") Integer baivietid,
                                                       @Valid @RequestBody Chitietbantin chitietbantin) {
        Chitietbantin one = baivietRepository.getOne(baivietid);
        if(one == null) {
            return ResponseEntity.notFound().build();
        }
        one.setTieude(chitietbantin.getTieude());
        one.setNoidung(chitietbantin.getNoidung());
        one.setSolanxem(chitietbantin.getSolanxem());

        Chitietbantin updatechititetbantin = baivietRepository.save(one);
        return ResponseEntity.ok(updatechititetbantin);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Chitietbantin> deleteBaiviet(@PathVariable(value = "id") Integer id) {
        Chitietbantin chitietbantin = baivietRepository.getOne(id);
        if(chitietbantin == null) {
            return ResponseEntity.notFound().build();
        }

        baivietRepository.delete(chitietbantin);
        return ResponseEntity.ok().build();
    }
}

package com.vuman00.ex11.api;

import com.vuman00.ex11.model.Car;
import com.vuman00.ex11.model.MediumCar;
import com.vuman00.ex11.model.ModermCar;
import com.vuman00.ex11.model.OldCar;
import com.vuman00.ex11.repository.CarRepository;
import com.vuman00.ex11.repository.MediumCarRepository;
import com.vuman00.ex11.repository.ModermCarService;
import com.vuman00.ex11.repository.OldCarRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/car")
public class CarApi {
    public static Logger logger = LoggerFactory.getLogger(BaivietApi.class);

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private  OldCarRepository oldCarRepository;
    @Autowired
    private  MediumCarRepository mediumCarRepository;
    @Autowired
    private  ModermCarService modermCarService;


    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<List<Car>> listAllCar() {
        List<Car> cars= carRepository.findAll();
        if (cars.isEmpty()) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Car>>(cars, HttpStatus.OK);
    }

    @RequestMapping(value = "/moderm", method = RequestMethod.GET)
    public ResponseEntity<List<ModermCar>> listModermCar() {
        List<ModermCar> modermCars = modermCarService.findAll();
        if (modermCars .isEmpty()) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<ModermCar>>(modermCars , HttpStatus.OK);
    }
    @RequestMapping(value = "/medium", method = RequestMethod.GET)
    public ResponseEntity<List<MediumCar>> listMediumCar() {
        List<MediumCar> mediumCars= mediumCarRepository.findAll();
        if (mediumCars .isEmpty()) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<MediumCar>>(mediumCars , HttpStatus.OK);
    }
    @RequestMapping(value = "/old", method = RequestMethod.GET)
    public ResponseEntity<List<OldCar>> listOldCar() {
        List<OldCar> oldCars = oldCarRepository.findAll();
        if (oldCars .isEmpty()) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<OldCar>>(oldCars , HttpStatus.OK);
    }



}

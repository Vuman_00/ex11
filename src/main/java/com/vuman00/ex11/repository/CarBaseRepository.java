package com.vuman00.ex11.repository;

import com.vuman00.ex11.model.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;


@NoRepositoryBean
public interface CarBaseRepository<T extends Car> extends JpaRepository<T,Integer> {
}

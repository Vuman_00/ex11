package com.vuman00.ex11.repository;

import com.vuman00.ex11.model.MediumCar;
import com.vuman00.ex11.model.ModermCar;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MediumCarRepository extends CarBaseRepository<MediumCar> {
}

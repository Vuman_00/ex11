package com.vuman00.ex11.repository;
import com.vuman00.ex11.model.OldCar;


public interface OldCarRepository extends CarBaseRepository<OldCar> {
}

package com.vuman00.ex11.repository;

import com.vuman00.ex11.model.Chitietbantin;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BaivietRepository extends JpaRepository<Chitietbantin,Integer> {

}
